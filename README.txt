CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------
The SolidOpinion commenting platform replaces your Drupal comment system with 
your comments hosted and powered by SolidOpinion. 
It replaces native Drupal commenting system.

SolidOpinion is a dynamic new commenting platform that can turn an ordinary 
discussion into a self-moderating community that promotes your content 
and generates revenue for your site.

SolidOpinion for Drupal

The SolidOpinion for Drupal plugin is easy to integrate even if you’re not 
a tech-head. And it has a smart design that adapts to your look and feel.

 * Uses the SolidOpinion API
 * Comments indexable by search engines (SEO-friendly)
 
 SolidOpinion Features

 * Intuitive and logical organization of comments and replies
 * Notifications by email
 * Built-in moderation and admin tools
 * Robust user-interface
 * Multi-likes and dislikes, user ranks and more
 * Social-sharing incentives that generate traffic
 * Powerful analytics and reporting
 * The Social game at the core of SolidOpinion is very flexible - you can 
 create your own system based on our templates
 * Personalized support for SolidOpinion users.
 * Generate new traffic with powerful social sharing incentives
 
More documentation http://help.solidopinion.com/

REQUIREMENTS
------------
There are no modules require

RECOMMENDED MODULES
-------------------
 * Markdown filter (https://www.drupal.org/project/markdown):
   When the enabled, display of the project's README.md help will be
   rendered with markdown.

INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
 * !Notice: After installation native comments will no shown

CONFIGURATION
-------------
To configure To configure SolidOpinion Comments Widget 
 * go to Administration > Configuration > Web Services > Solidopinion
 * Login or register new user.
 * Choose title and shortname 
 (shortname ﻿is unique identifier for your site in SolidOpinion)
 * Add new integration. 
 * Now you can change plugin﻿ appearance, colour schema etc.
 * To activate SolidOpinion comments widget on your site 
 click "Make this integration active on your site" link.
 * Go to your site and enjoy your new comments.
To get the comments back, disable the module and clear caches.

TROUBLESHOOTING
---------------
 * If you have any problem with module please 
  - visit http://help.solidopinion.com/
  - contact us by live chat on http://solidopinion.com/ 
  - or send us email to support@solidopinion.com.

FAQ
---
Q: Am I able to keep my existing comments?
A: Yes, to import comments from other commenting systems please contact us 
by live chat on http://solidopinion.com/ 
or send us email to support@solidopinion.com.

MAINTAINERS
-----------
Current maintainers:
 * SolidOpinion http://solidopinion.com/ - https://www.drupal.org/u/solidopinion
